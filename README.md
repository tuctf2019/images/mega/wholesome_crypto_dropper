# Wholesome Crypto (Dropper) -- Mega -- TUCTF2019

This is the server that distributes the cryptogram.

[Source](https://gitlab.com/tuctf2019/mega/wholesome_crypto_dropper)

## Chal Info

Desc: `Thought you might need some wholesome crypto.`

Prereq: `rop me like a hurricane`

Flag: `TUCTF{M4NY_ST3PS_3QU4LS_B1G_R3W4RD}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/wholesome_crypto_dropper)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/wholesome_crypto_dropper:tuctf2019
```

