
FROM python:3.6-alpine

WORKDIR /usr/src/app

COPY ./src .

CMD /usr/src/app/dropper.py
